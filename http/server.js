var http = require("http");

http.createServer((request, response) => {
    console.log('HIT', Date.now());
    
    response.writeHead(200, { 'Content-Type': 'text/plain' });

    response.end('Hello World\n');
    console.log('Returned hello');
}).listen(8081);

console.log('Server running on port 8081');

// Ensure it dies in all envs
process.on('SIGINT', () => {
    process.exit()
})
