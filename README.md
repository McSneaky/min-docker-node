# Small NodeJS servers Dockerized

> **NOTE!** It is not following **any** best practices! **DO NOT** use this project as any sample or as a base to production code

Basic WebSocket and HTTP servers, all they do is return "Hello world" and do some timestamped logging

Container for http server: registry.gitlab.com/mcsneaky/min-docker-node:http-master

Container for WS server: registry.gitlab.com/mcsneaky/min-docker-node:ws-master
