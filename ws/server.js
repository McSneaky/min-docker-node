const WebSocket = require('ws');

const wss = new WebSocket.Server({
    host: '0.0.0.0',
    port: 8082
});

wss.on('connection', (ws) => {
    console.log('Connected at', Date.now());
    
    ws.on('message', (message) => {
        console.log('received: %s', message);
        ws.send('Got: ', message)
    });
    ws.send('something');
});

// Ensure it dies in all envs
process.on('SIGINT', () => {
    process.exit()
})
